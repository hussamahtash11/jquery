$(document).ready(function(){
    $("#btns").click(function(){
      $("#mobile-menu").toggleClass("hidden");
    });

    $("#user-menu-button").click(function(){
        $("#profile-menu").toggleClass("hidden");
      });


      
      $.get("https://jsonplaceholder.typicode.com/users", function(data, status){
          console.log(data);

          data.forEach((person) => {
              
          
        //for (const i in data) {})
            $("#tab").append(`<tr>
            <td class="px-6 py-4 whitespace-nowrap">
              <div class="flex items-center">
                <div class="flex-shrink-0 h-10 w-10">
                  <img class="h-10 w-10 rounded-full" src="https://i.pravatar.cc/300`+person.id+`" alt="">
                </div>
                <div class="ml-4">
                  <div class="text-sm font-medium text-gray-900">` + person.name + `</div>
                  <div class="text-sm text-gray-500">`+ person.email +`</div>
                </div>
              </div>
            </td>
            <td class="px-6 py-4 whitespace-nowrap">
              <div class="text-sm text-gray-900">`+ person.address.city +`</div>
              <div class="text-sm text-gray-500">`+ person.address.zipcode +`</div>
            </td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
              <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
              `+ person.website +`
                </span>
            </td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">`+ person.phone +`</td>
            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
              <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
            </td>
          </tr>`)

        })
      });
    
  });
// console.log('Votre code ici')
// name
// email
// adress.city & zipcode
// website 
// phone